const { version: appVersion } = require('../../package.json');
const startupMode = require('../enums/mode');
const axios = require('axios');
const os = require('os');

class TelemetryManager {
    errors = 0;

    constructor({ host = "api-tools.starserv.ru", runAsMode = startupMode.ALL_FILES } = {}) {
        this.host = host;
        this.runAsMode = runAsMode;
        this.timestamp = {
            applicationStart: new Date(),
            scan: {
                start: 0,
                end: 0
            },
            decrypt: {
                start: 0,
                end: 0
            },
            save: {
                start: 0,
                end: 0
            }
        }
        this.files = {
            decrypt: 0
        }
        this.game = {
            title: null, // encrypted key from system.json
            key: null,   // title from system.json
        }

        // ? collect only OS platform and version, do not collect any else data like ip, username, passwords and etc.
        this.platform = os.platform?.();
        this.version = os.version?.();

        this.sendStartup();
    }

    get baseUrl() {
        return `http://${this.host}/v1/telemetry`
    }

    /*  */

    get startupEntryPoint() {
        return `startup`
    }

    get filesEntryPoint() {
        return `files`
    }

    /*  */

    get startupData() {
        return {
            platform: this.platform,
            version: this.version,
            app_version: appVersion,
            runAsMode: this.runAsMode
        }
    }

    get filesData() {
        return {
            timestamp: this.timestamp,
            files: this.files,
            game: this.game
        }
    }

    /*  */

    sendStartup() {
        axios({
            method: "post",
            url: `${this.baseUrl}/${this.startupEntryPoint}`,
            data: this.startupData
        }).catch(() => {
            if (this.errors <= 10) {
                this.sendStartup();
                this.errors++;
            }
        })
    }

    sendFiles() {
        axios({
            method: "post",
            url: `${this.baseUrl}/${this.filesEntryPoint}`,
            data: this.filesData
        }).catch(() => {
            if (this.errors <= 10) {
                this.sendFiles();
                this.errors++;
            }
        })
    }

    /*  */

    startScan() {
        this.timestamp.scan.start = new Date();
    }

    completeScan() {
        this.timestamp.scan.end = new Date();
    }

    /*  */

    startDecrypt() {
        this.timestamp.decrypt.start = new Date();
    }

    // too lazy for do better :c
    completeDecrypt(files = 0, title, key) {
        this.timestamp.decrypt.end = new Date();
        this.files.decrypt = files;
        this.game.title = title;
        this.game.key = key;
    }

    /*  */

    startSave() {
        this.timestamp.save.start = new Date();
    }

    completeSave() {
        this.timestamp.save.end = new Date();
    }
}

module.exports = TelemetryManager;