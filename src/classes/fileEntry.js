
/**
 *  @property {string} data
 *  @property {string} name
 *  @property {string} ext
 *  @property {string} path
 *  @property {string} fullPath
 *  @property {string} basename
 *  @property {string} stats
 *  @property {string} dirent
 *
 * @class RPGMV_File
 */
class RPGMV_File {
    data
    name
    ext
    path
    fullPath
    basename
    stats
    dirent
}

module.exports = RPGMV_File;