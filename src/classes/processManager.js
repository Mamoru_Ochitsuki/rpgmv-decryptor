class ProcessManager {
    titles = [
        /* 0 */ "RPGMV Decrypter",
        /* 1 */ "RPGMV Decrypter [Scanning files...]",
        /* 2 */ "RPGMV Decrypter [Decrypting files...]",
        /* 3 */ "RPGMV Decrypter [Saving files...]",
        /* 4 */ "RPGMV Decrypter [Complete]"
    ];

    status = 0;

    constructor() {
        this.updateTitle();
    }

    updateTitle() {
        process.title = this.titles[this.status++];
    }
}

module.exports = ProcessManager;