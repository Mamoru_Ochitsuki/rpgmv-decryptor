const progress = require('cli-progress');

class ProgressManager {
    constructor() {
        this.progressBar = new progress.SingleBar({
            format: 'RPGMV Decrypter | {bar} | {percentage}% | {value}/{total} Files | {duration}s',
            stopOnComplete: true
        }, progress.Presets.shades_classic);
    }

    create() {
        this.progressBar.start(0, 0);
    }

    update(totalValue, startValue) {
        if (totalValue > 0) this.progressBar.start(totalValue, startValue);
        else {
            this.progressBar.start(0, 0);
            this.progressBar.stop();
        }
    }

    incrementCallback() {
        return this.progressBar.increment.bind(this.progressBar);
    }

    setTotalCallback() {
        return this.progressBar.setTotal.bind(this.progressBar);
    }

}

module.exports = ProgressManager;