const readdirp = require('readdirp');
const fs = require('fs');
const path = require('path');
const defaultIgnoreFolders = require('../enums/defaultIgnoreFolders');

class FilesManager {

    /**
     *
     * @type {import("./fileEntry")[]}
     * @memberof FilesManager
     */
    files = [];

    constructor({
        path = ".",
        storage = "decrypted",
        depth = 5,
        fileFilter = [],
        directoryFilter = [] } = {}) {
        this.path = path;
        this.storage = storage;
        this.scanOptions = {
            depth: depth,
            fileFilter: fileFilter,
            directoryFilter: [...defaultIgnoreFolders, ...directoryFilter]
        }
    }

    async readDir(callback) {
        for await (const entry of readdirp(this.path, this.scanOptions)) {
            const data = fs.readFileSync(entry.fullPath)
            const { name, ext } = path.parse(entry.basename);

            const file = {
                ...entry,
                data,
                name,
                ext
            }

            this.files.push(file);

            callback?.(this.files.length);
        }
    }

    async saveFiles(callback) {
        for (const file of this.files) {
            const relative = path.parse(path.relative(this.path, file.fullPath)).dir;
            const pathToStorage = path.join(this.storage, relative);
            const pathToStorageFile = path.join(pathToStorage, `${file.name}${file.ext}`);

            fs.mkdirSync(pathToStorage, { recursive: true });
            fs.writeFileSync(pathToStorageFile, Buffer.from(file.data));

            callback?.(); // ? actually used for draw progress bar 
        }
    }
}

module.exports = FilesManager;
