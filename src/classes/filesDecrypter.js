const Decrypter = require('./Decrypter.js');
const fileTypes = require('../enums/fileTypes.js');
const files = require('../enums/files.js');
const mode = require('../enums/mode.js');

class FileDecrypter {

    /**
     * Creates an instance of FileDecrypter.
     * @param {import("./fileEntry")[]} files
     * @memberof FileDecrypter
     */
    constructor(files = []) {
        this.files = files;
        this.decrypter = new Decrypter();
    }

    async decryptByMode(runAsMode) {
        if (mode.ALL_FILES === runAsMode) this.decryptAllFilesWithKey();
        if (mode.IMAGES_ONLY === runAsMode) this.decryptAllPNGFiles();
        return this;
    }

    async decryptAllFilesWithKey() {
        const systemJson = this.files.find(file => file.basename.toLowerCase() === files.system[1]);

        if (systemJson) {
            // console.log("systemJson.data", systemJson.data.toString());

            try {
                const { encryptionKey, gameTitle } = JSON.parse(systemJson.data.toString());
                this.gameTitle = gameTitle ? gameTitle : null;
                this.key = encryptionKey ? encryptionKey : null;
            } catch (error) {
                // todo: add try catch inside try catch?
                const json = JSON.parse(systemJson.data.toString().match(/(\{|\[).*(\}|\])/)[0]); // this should fix invalid json in data.json file... sometimes 
                const { encryptionKey, gameTitle } = json;
                this.gameTitle = gameTitle ? gameTitle : null;
                this.key = encryptionKey ? encryptionKey : null;
            }

            if (this.key) {
                this.decrypter = new Decrypter(this.key);

                for (const file of this.files) {
                    if (file.ext !== this.getDecryptedType(file.ext)) {
                        // if file.ext === decryptedFile.ext then file isn't decrypted (pure but ... simple)
                        this.decryptFile(file);
                    }
                }
            }
            else this.decryptAllPNGFiles()
        }
        else this.decryptAllPNGFiles()
        // todo: maybe change to something else?

    }

    async decryptAllPNGFiles() {
        for (const file of this.files) {
            if (this.isEncryptedPNGFile(file))
                this.decryptPNGFile(file)
        }
    }

    /**
     *
     *
     * @param {import("./fileEntry")} file
     * @memberof FileDecrypter
     */
    decryptPNGFile(file) {
        file.data = this.decrypter.restorePngHeader(file.data);
        file.ext = fileTypes.decrypted.PNG;
        return file;
    }

    decryptFile(file) {
        file.data = this.decrypter.decrypt(this._toArrayBuffer(file.data));
        file.ext = this.getDecryptedType(file.ext);
        return file;
    }

    /**
     *
     *
     * @param {import("./fileEntry")} file
     * @return {boolean} encrypted (true)_ or decrypted (false) 
     * @memberof FileDecrypter
     */
    isEncryptedPNGFile(file) {
        return fileTypes.encrypted.PNG.includes(file.ext)
    }

    getDecryptedType(ext) {
        if (fileTypes.encrypted.PNG.includes(ext)) return fileTypes.decrypted.PNG;
        if (fileTypes.encrypted.M4A.includes(ext)) return fileTypes.decrypted.M4A;
        if (fileTypes.encrypted.OGG.includes(ext)) return fileTypes.decrypted.OGG;
        return ext
    }

    /*  */

    _toArrayBuffer(buf) {
        const ab = new ArrayBuffer(buf.length);
        const view = new Uint8Array(ab);
        for (let i = 0; i < buf.length; ++i) {
            view[i] = buf[i];
        }
        return ab;
    }
}

module.exports = FileDecrypter;