const fileTypes = {
    encrypted: {
        PNG: [".rpgmvp", ".png_"],
        M4A: [".rpgmvm", ".m4a_"],
        OGG: [".rpgmvo", ".ogg_"],

    },
    decrypted: {
        PNG: ".png",
        M4A: ".m4a",
        OGG: ".ogg"
    },
}

module.exports = fileTypes;