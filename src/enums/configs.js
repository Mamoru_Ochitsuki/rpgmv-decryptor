const fileTypes = require('./fileTypes');
const files = require('./files');
const mode = require("./mode");

module.exports = {
    [mode.ALL_FILES]: {
        fileFilter: [
            ...fileTypes.encrypted.PNG.map(fileType => `*${fileType}`),
            ...fileTypes.encrypted.OGG.map(fileType => `*${fileType}`),
            ...fileTypes.encrypted.M4A.map(fileType => `*${fileType}`),
            ...files.system
        ]
    },
    [mode.IMAGES_ONLY]: {
        fileFilter: fileTypes.encrypted.PNG.map(fileType => `*${fileType}`)
    }
}