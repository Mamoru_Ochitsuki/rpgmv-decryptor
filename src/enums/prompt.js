const startupMode = require('./mode');

module.exports = {
    type: "select",
    name: "value",
    message: "Choose startup mode",
    choices: [
        { title: "Decrypt All Files", description: "App would decrypt all file types (rpgmvp, rpgmvo, rpgmvm etc)", value: startupMode.ALL_FILES },
        { title: 'Decrypt Only Images', description: "App would decrypt only image files (rpgmvp)", value: startupMode.IMAGES_ONLY },
        { title: 'Encrypt All Files', disabled: true },
    ],
    initial: 0
}