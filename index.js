const TelemetryManager = require('./src/classes/telemetryManager');
const ProgressManager = require('./src/classes/progressManager');
const FilesDecrypter = require('./src/classes/filesDecrypter');
const ProcessManager = require('./src/classes/processManager');
const FileManager = require('./src/classes/filesManager');
const configs = require('./src/enums/configs');
const prompt = require('./src/enums/prompt');
const prompts = require('prompts');

async function bootstrap() {
    const { value: runAsMode } = await prompts(prompt)

    if (runAsMode >= 0) runApp(runAsMode, configs[runAsMode])
}

bootstrap();

async function runApp(runAsMode, config) {
    const telemetryManager = new TelemetryManager({ runAsMode });
    const progressManager = new ProgressManager();
    const processManager = new ProcessManager();

    // Create progress bar
    progressManager.create();
    processManager.updateTitle();

    // Scan files
    telemetryManager.startScan()
    const fileManager = new FileManager(config);
    await fileManager.readDir(progressManager.setTotalCallback());
    telemetryManager.completeScan()

    // update progress bar 
    progressManager.update(fileManager.files.length);
    processManager.updateTitle();

    // Decrypt files
    telemetryManager.startDecrypt()
    const decrypter = await new FilesDecrypter(fileManager.files).decryptByMode(runAsMode);
    telemetryManager.completeDecrypt(fileManager.files.length, decrypter?.gameTitle, decrypter?.key)

    processManager.updateTitle();

    // Save files
    telemetryManager.startSave();
    await fileManager.saveFiles(progressManager.incrementCallback());
    telemetryManager.completeSave();

    processManager.updateTitle();
    telemetryManager.sendFiles();

    setTimeout(() => { }, 6e4) // close app after 1 minute
}