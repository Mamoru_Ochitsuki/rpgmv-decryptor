# **Changelog**

All changes would be writed here.
<!-- ## **[Future Updates] - XX.XX.202X**

### **Features**

- Copy all **not encrypted** types of files (like png, ogg etc) in `decrypted/` folder, for fast access.
- Encrypting Files with key and store in `encrypted/` or `re-encrypted/` folder. -->

## **[0.8.7] - 22.02.2021**

### **Bug Fix**

- Fix bug with parsing json file (`system.json`). 
  - This file have a few symbols (on start and end of json file) and that could crash application. Currently should be fixed.

## **[0.8.6] - 04.11.2021**

### **Features**

- Now all modes (decrypting only images or all files) allow start from one binary file.

### **Fixes**

- Small optimisations of code.
- Add a bit more telemetry data.

## **[0.7.6] - 04.09.2021**

### **Fixes**

- add version type to sending telemety (now version of app would be sended with `png` or `all` postfix)

## **[0.7.5] - 04.09.2021**

### **Features**

- support `.ogg` - `.ogg_` and `.rpgmvo` files.
- support `.m4a` - `.m4a_` and `.rpgmvm` files.

### **Fixed**

- fix bug with sending telemetry 

## **[0.6.4] - 03.09.2021**

### **Added**

- add `app version` for telemetry (just for know what version was used by users);
- add `count of decrypted files` for telemetry (only count, **do not getting names or data of files**);
- add `scan`, `decrypt` and `save` spend time for telemetry (just for know spend time for each stage);

## **[0.6.3] - 02.09.2021**

### **Changes**

- fix bug of sending telemetry for darwin (macos). 

## **[0.6.2] - 02.09.2021**

### **Changes**

- now for sending telemetry would be used `https` protocol instead of `http`.

## **[0.6.1] - 10.08.2021**

### **Added**

- add telemetry

> Do not afraid, application collect only platform (linux, windows etc) and version (windows 7, 10 etc) of current OS, **app do not collect any else data like passwords or etc**.

## **[0.5.1] - 10.08.2021**

### **Fixed**

- fixed bug with progress bar

## **[0.5.0] - 10.08.2021**

### **Added**

- add app status titles:
  - `[Scanning files...]` - application **scan** current folder and subfolders
  - `[Decrypting files...]` - application **decrypt files** (that is very fast)
  - `[Saving files...]` - application **save files** in `decrypted/` folder (relative from disk speed I/O)
  - `[Complete]`  - application **complete** work and will close after 60 seconds.

> Do not forget check [**new examples**](/../../wikis/docs/Examples).

## **[0.4.0] - 07.08.2021**

> Application was renamed (from `DecryptAllPng` to `PNG_Decrypter`) and rewrited.

### **Added**

- add new **PNG** file types for check (`.png_`), now - `.png_` files would be decrypted too. 
- add **progress bar** with count of `decrypted` and `total` files, `spend time` etc. 
  - progress bar have two status
    - scanning
    - decrypting

### **Improved**

- improve speed optimisation for save and read files `[beta]`.
- improve disk **i/o** optimisation for saving files `[beta]`. 

### **Removed**

- remove support for **drag&drop** 
- remove support for **multi workspaces**

> Note: you also can use previus version(s) if you want use **drag&drop** or/and **multi workspaces** functions. Also, if many users request this feature in new versions I will add it in future, maybe.

## **[0.3.2] - 28-01-2021**

### **Added**

- support for drag&droped **folders** (just drag and drop any folder(s) on the binary file)
- support for drag&droped **files** (application would check current folder of file, be aware!)
- new screenshot for 0.3.2 version of app in [**examples**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/wikis/Examples)

## **[0.2.2] - 26-01-2021**

### **Added**

- a bit more debug
- new screenshot for 0.2.2 version of app in [**examples**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/wikis/Examples)

### **Changes**

- a bit-tiny-small code review

## **[0.2.1] - 10-01-2021**

Note: with this fixes, if you find any "trouble file" you can say what file would be failed.

### **Added**

- add file name what decripting in to counter of decrypted files (x of total) for better debuging

### **Changes**

- small code review
- change a few phrases

## **[0.2.0] - 02-01-2021**

### **Added**

- macos binaries support (`-macos`)
- macos binaries virustotal check

### **Changed**

- download [**releases**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/releases) now do not require auth with gitlab account

## **[0.1.4] - 01-01-2021**

### **Added**

- virustotal binaries check
  - win os binary check
  - lin os binary check

## **[0.1.2] - 27-12-2020**

### **Changed**

- optimisations in file parser
- small beauty in code style 

## **[0.1.1] - 27-12-2020**

### **Changed**

- fix bug with parsing file extension
- new method for parsing file extension

## **[0.0.1] - 19-12-2020**

### **Added**

- support new files extensions [`.rpgmvp`]
