const paths = {
    storage: "decrypted",
    encryptedPNGFile: "test/assets/png/encrypted.rpgmvp",
    decryptedPNGFile: "decrypted/test/assets/png/encrypted.png"
}

module.exports = paths;