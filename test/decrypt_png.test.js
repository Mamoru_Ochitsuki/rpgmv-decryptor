const FilesDecrypter = require('../src/classes/filesDecrypter');
const FileManager = require('../src/classes/filesManager');
const fileTypes = require('../src/enums/fileTypes');
const fileMD5 = require('./enums/fileMD5');
const paths = require('./enums/paths');
const md5 = require('md5');
const fs = require('fs');

const config = { fileFilter: fileTypes.encrypted.PNG.map(fileType => `*${fileType}`) }

beforeAll(() => {
    fs.rmdirSync(paths.storage, { recursive: true })
});

afterAll(() => {
    fs.rmdirSync(paths.storage, { recursive: true })
});

test("Encrypted file should exist before decrypt", () => {
    expect(fs.existsSync(paths.encryptedPNGFile)).toBeTruthy();
});

test("Decrypted file should not exist before decrypt", () => {
    expect(fs.existsSync(paths.decryptedPNGFile)).toBeFalsy();
});

test("Decrypted storage should not exist before decrypt", () => {
    expect(fs.existsSync(paths.storage)).toBeFalsy();
});

test('FileManager should find one file', async () => {
    const fileManager = new FileManager(config);
    await fileManager.readDir();

    expect(fileManager.files.length).toBe(1);
});

test('FilesDecrypter should decrypt one file', async () => {
    const fileManager = new FileManager(config);
    await fileManager.readDir();

    await new FilesDecrypter(fileManager.files).decryptAllPNGFiles();

    expect(fileManager.files[0].ext === fileTypes.decrypted.PNG).toBeTruthy();
});

test('FileManager should save one file', async () => {
    const fileManager = new FileManager(config);
    await fileManager.readDir();
    await new FilesDecrypter(fileManager.files).decryptAllPNGFiles();
    await fileManager.saveFiles();

    expect(fs.existsSync(paths.decryptedPNGFile)).toBeTruthy();
});

test('FilesDecrypter::isEncryptedPNGFile should return encrypted status for every encrypted png file type', () => {
    for (const ext of fileTypes.encrypted.PNG) {
        expect(new FilesDecrypter().isEncryptedPNGFile({ ext })).toBeTruthy();
    }
});

test('FilesDecrypter::isEncryptedPNGFile should return decrypted status for every decrypted png file type', () => {
    expect(new FilesDecrypter().isEncryptedPNGFile({ ext: fileTypes.decrypted.PNG })).toBeFalsy();
});

test('Encrypted and decrypted files should have different md5 hash', async () => {
    const decryptedPNGFileData = fs.readFileSync(paths.decryptedPNGFile);
    const encryptedPNGFileData = fs.readFileSync(paths.encryptedPNGFile);

    expect(md5(decryptedPNGFileData) === md5(encryptedPNGFileData)).toBeFalsy();
});

test('Decrypted file should be the same as saved md5 hash', async () => {
    const decryptedPNGFileData = fs.readFileSync(paths.decryptedPNGFile);

    expect(md5(decryptedPNGFileData) === fileMD5.decryptedPNGFile).toBeTruthy();
});

test('Encrypted file should be the same as saved md5 hash', async () => {
    const encryptedPNGFileData = fs.readFileSync(paths.encryptedPNGFile);

    expect(md5(encryptedPNGFileData) === fileMD5.encryptedPNGFile).toBeTruthy();
});