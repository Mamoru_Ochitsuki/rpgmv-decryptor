const fileTypes = require('../src/enums/fileTypes');
const defaultIgnoreFolders = require('../src/enums/defaultIgnoreFolders');

describe('FileTypes group', () => {
    test('FileTypes.encrypted.PNG should be expected  array ', () => {
        expect(fileTypes.encrypted.PNG).toMatchObject([".rpgmvp", ".png_"]);
    });

    test('FileTypes.encrypted.OGG should be expected  array ', () => {
        expect(fileTypes.encrypted.OGG).toMatchObject([".rpgmvo", ".ogg_"]);
    });

    test('FileTypes.encrypted.M4A should be expected array', () => {
        expect(fileTypes.encrypted.M4A).toMatchObject([".rpgmvm", ".m4a_"]);
    });

    test('FileTypes.decrypted.PNG should be equal png', () => {
        expect(fileTypes.decrypted.PNG).toBe(".png");
    });

    test('FileTypes.decrypted.OGG should be equal png', () => {
        expect(fileTypes.decrypted.OGG).toBe(".ogg");
    });

    test('FileTypes.decrypted.M4A should be equal png', () => {
        expect(fileTypes.decrypted.M4A).toBe(".m4a");
    });
});

describe('defaultIgnoreFolders group', () => {
    test('FileTypes should be object ', () => {
        expect(defaultIgnoreFolders).toMatchObject(["!.git", "!node_modules", "!decrypted"]);
    });
});