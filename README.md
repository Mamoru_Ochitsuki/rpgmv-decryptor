# **RPGMV Decrypter**

[![release version](https://badgen.net/gitlab/release/BlackYuzia/rpgmv-decryptor)](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/releases)
[![releases count](https://badgen.net/gitlab/releases/BlackYuzia/rpgmv-decryptor)](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/releases)
[![tags count](https://badgen.net/gitlab/tags/BlackYuzia/rpgmv-decryptor)](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/tags)
[![pipeline status](https://gitlab.com/BlackYuzia/rpgmv-decryptor/badges/master/pipeline.svg)](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/pipelines)
[![last commit](https://badgen.net/gitlab/last-commit/BlackYuzia/rpgmv-decryptor/master)](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/commits/master)

## **Description**

This application allow you decrypt encrypted **rpgmv** files. 

**Currently application have two versions for [support only images (`PNG`)](../../wikis/docs/Difference) and [support all file types (`ALL`)](../../wikis/docs/Difference)**.

## **Docs**

- [**Home**](../../wikis/Home)
- [**Features**](../../wikis/docs/Features)
- [**Install**](../../wikis/docs/Install)
- [**ALL vs PNG**](../../wikis/docs/Difference)
- [**Usage**](../../wikis/docs/Usage)
- [**Examples**](../../wikis/docs/Examples)
- [**Compare with other decrypters**](../../wikis/docs/Compare)
- [**FAQ**](../../wikis/docs/FAQ)
- [**Changelog**](CHANGELOG.md)

## **Support**

- [**Discord Channel**](https://discord.io/KEYSDEV)
- [**Gitlab Issues**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/issues)

## **Credits**

- [**Petschko**](https://github.com/Petschko) - for decrypting methods.