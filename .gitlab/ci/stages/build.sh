#!/bin/bash

# Load variables
source .gitlab/ci/variables/base.sh

# Job commands
npm install
npm run build:ci -- -o "$binary_path"
