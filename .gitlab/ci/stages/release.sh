#!/bin/bash

# Load Functions
source .gitlab/ci/functions/release.sh # imported generate_package_asset, generate_url_asset, install_release_cli

# Load Variables
source .gitlab/ci/variables/base.sh            # imported binaries_array, binaries_names
source .gitlab/ci/variables/release.sh         # imported virustotal_file_url
source .gitlab/ci/storage/virustotal.storage   # imported md5_array
source .gitlab/ci/storage/download-url.storage # imported package_urls

# Init Variables
declare -xa package_assets=()

# Add package files (binaries)
for index in "${!binaries_array[@]}"; do
    package_assets+=("$(generate_package_asset "${binaries_array[$index]}" "${package_urls[$index]}")")
done

# Add package virustotal scans
for index in "${!md5_array[@]}"; do
    package_assets+=("$(generate_url_asset "VT ${binaries_names[$index]}" "${virustotal_file_url}/${md5_array[$index]}")")
done

install_release_cli # install before use
release-cli \
create \
--name $CI_COMMIT_TAG \
--tag-name $CI_COMMIT_TAG \
--description "Check [**changelog**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/blob/master/CHANGELOG.md) for see changes of current version." \
--assets-link "${package_assets[0]}" \
--assets-link "${package_assets[1]}" \
--assets-link "${package_assets[2]}" \
--assets-link "${package_assets[3]}" \
--assets-link "${package_assets[4]}" \
--assets-link "${package_assets[5]}"
