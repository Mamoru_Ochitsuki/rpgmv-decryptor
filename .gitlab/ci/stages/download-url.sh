#!/bin/bash

# Load Functions
source .gitlab/ci/functions/download-url.sh

# Init Variables
declare -x package_id
declare -xa package_files=()
declare -xa package_urls=()

package_id=$(get_package_id)
package_files=($(get_package_files "$package_id"))

# Parse package urls
for file_id in "${package_files[@]}"; do
    package_urls+=("$(get_download_url "$file_id")")
done

# Storage
echo "declare -xar package_urls=("${package_urls[@]}")" >.gitlab/ci/storage/download-url.storage # store
