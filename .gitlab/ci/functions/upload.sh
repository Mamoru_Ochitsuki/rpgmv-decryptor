#!/bin/bash

upload_file() {
    local path_to="$1"
    local binary_name="$2"
    local path_to_binary="${path_to}/${binary_name}"

    curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${path_to_binary}" "${package_registry_url}/${binary_name}"
}

export -f upload_file
